flow3r api
==========

A little monolithic API service for accessing stuff about the flow3r badge,
especially release information.

Running
-------

    $ go run .

APIs
----

| Endpoint                                | Description                                             |
|-----------------------------------------|---------------------------------------------------------|
| `/api/releases.json`                    | WebFlasher compatible list of releases                  |
| `/api/release/<version>/<artifact.bin>` | Artifacts from releases, pointed to by `/releases.json` |

Internals
---------

`releases.json` is generated on demand from a cached view from the GitLab API.
Currently up to 50 releases are supported, as pagionation hasn't yet been
implemented.

All artifacts are cached in memory after being extracted from tarballs. Might
need to be offloaded to some other storage.


Prod
----

Runs on The GitLab Chonker.

    .--------------------------.
    | nginx                    |
    |  .---------------------. |
    |  | VHOST               | |
    |  |---------------------| |
    |  | flow3r.garden       | |
    |  |  .----------------. | |     .----------.
    |  |  | location /     |-------> | GL Pages |
    |  |  '----------------' | |     '----------'
    |  |  .----------------. | |     .------------.
    |  |  | location /api  |-------> | flow3r-api |---> git.flow3r.garden
    |  |  '----------------' | |     '------------'
    |  '.....................' |
    '--------------------------'

Licensing
---------

Copyright (c) 2023 Flow3r Authors.

Licensed under the EUPL, see COPYING.
